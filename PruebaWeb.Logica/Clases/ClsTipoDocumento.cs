﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace PruebaWeb.Logica.Clases
{
    public static class ClsTipoDocumento
    {
        static Utilidades.Response response = null;


        /// <summary>
        /// Metodo creta Tipo de documento
        /// </summary>
        /// <param name="tipoDocumento"></param>
        /// <returns>response</returns>
        public static  Utilidades.Response  CreateTipoDocumento(Entidades.tbTipoDocumento tipoDocumento)
        {
            response = new Utilidades.Response();
            try
            {
                using (Entidades.dbPruebaWebEntities db= new Entidades.dbPruebaWebEntities())
                {

                    db.tbTipoDocumento.Add(tipoDocumento);
                    db.SaveChanges();

                    response.Status = true;
                    response.Message = "Registro exitoso";
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;   
            }
            return response;
        }


        /// <summary>
        /// Metodo Update de tipo de documento
        /// </summary>
        /// <param name="tipoDocumento"></param>
        /// <returns>response</returns>
        public static Utilidades.Response UpdateTipoDocumento(Entidades.tbTipoDocumento tipoDocumento)
        {

            response = new Utilidades.Response();

            try
            {

                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var item = db.tbTipoDocumento.Find(tipoDocumento.TipoDocumento_Id) ?? null;
                                        
                    if (item !=null)
                    {
                        item = tipoDocumento;                       
                        var x= db.SaveChanges();

                        response.Status = true;
                        response.Message = "Actualizacion exitosa";
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "No se encontro el resgistro";
                    }
                    
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
                
            }
            return response;
        }

        /// <summary>
        /// Metodo de delete de tipo de documento
        /// </summary>
        /// <param name="Codigo"></param>
        /// <returns>response</returns>
        public static Utilidades.Response DeleteTipoDocumento(int Codigo)
        {
           response = new Utilidades.Response();
            try
            {
                using (Entidades.dbPruebaWebEntities db=new Entidades.dbPruebaWebEntities())
                {
                    var item = db.tbTipoDocumento.Find(Codigo) ?? null;

                    if (item !=null)
                    {
                        db.tbTipoDocumento.Remove(item);
                        db.SaveChanges();

                        response.Status = true;
                        response.Message = "Registro eliminado";

                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "No se encontro el registro";
                    }
                }
            }
            catch (Exception ex)
            {

                response.Status = false;
                response.Message = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Metodo de listar todos los tipos de documento
        /// </summary>
        /// <returns>response</returns>
        public static Utilidades.Response ListTipoDocumento()
        {
            response = new Utilidades.Response();
            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    response.Data= db.tbTipoDocumento.ToList();
                    response.Status = true;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }
            return response;
        }

        /// <summary>
        /// Metodo para cnsultar por codigo un registro en Tipo de documento
        /// </summary>
        /// <param name="Codigo"></param>
        /// <returns>response</returns>
        public static Utilidades.Response Consulta(int Codigo)
        {
            response = new Utilidades.Response();

            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var result = (from tbTipoDocumento in db.tbTipoDocumento
                                  where tbTipoDocumento.Codigo == Codigo
                                  select tbTipoDocumento).ToList();

                    response.Status = true;
                    response.Data = result.ToList();
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }
            return response;
        }

        /// <summary>
        /// Llena un gridView con todos los tipos de documento
        /// </summary>
        /// <param name="grid"></param>
        public static void LlenarGridTipoDocumento(ref GridView grid)
        {
            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var data = (from con in db.tbTipoDocumento
                                select new
                                {
                                    Codigo = con.Codigo,
                                    Nombre = con.Nombre,
                                    Descripcion = con.Descripcion

                                }).ToList();

                    grid.DataSource = data;
                    grid.DataBind();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Llena un DropDownList con el listado de los Roles
        /// </summary>
        /// <param name="ddl"></param>
        public static void llenarDropDownConDocumento(ref DropDownList ddl)
        {
            try
            {

                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var data = (from i in db.tbTipoDocumento
                                select new
                                {
                                    text = i.Nombre,
                                    value = i.TipoDocumento_Id

                                }).ToList();

                    ddl.DataSource = data;
                    ddl.DataTextField = "text";
                    ddl.DataValueField = "value";
                    ddl.DataBind();

                    ddl.Items.Insert(0, new ListItem("Seleccione Documento", "-1"));
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}

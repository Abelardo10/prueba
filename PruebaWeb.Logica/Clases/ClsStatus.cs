﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace PruebaWeb.Logica.Clases
{
   public static class ClsStatus
    {
        static Utilidades.Response response = null;

        /// <summary>
        /// Metodo create de Status
        /// </summary>
        /// <param name="status"></param>
        /// <returns>response</returns>
        public static Utilidades.Response CreateStatus(Entidades.tbStatus status)
        {
            response = new Utilidades.Response();
            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {

                    db.tbStatus.Add(status);
                    db.SaveChanges();

                    response.Status = true;
                    response.Message = "Registro exitoso";
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }
            return response;
        }

        /// <summary>
        /// Metodo Update de Status
        /// </summary>
        /// <param name="status"></param>
        /// <returns>response</returns>
        public static Utilidades.Response UpdateStatus(Entidades.tbStatus status)
        {
            response = new Utilidades.Response();

            try
            {

                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var item = db.tbStatus.Find(status.Status_Id) ?? null;

                    if (item != null)
                    {
                        item = status;
                        var x = db.SaveChanges();

                        response.Status = true;
                        response.Message = "Actualizacion exitosa";
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "No se encontro el resgistro";
                    }

                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;

            }
            return response;
        }


        /// <summary>
        /// Metodo delete a partir de un Status_id
        /// </summary>
        /// <param name="Status_Id"></param>
        /// <returns>response</returns>
        public static Utilidades.Response DeleteStatus(int status_Id)
        {
            response = new Utilidades.Response();
            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var item = db.tbStatus.Find(status_Id) ?? null;

                    if (item != null)
                    {
                        db.tbStatus.Remove(item);
                        db.SaveChanges();

                        response.Status = true;
                        response.Message = "Registro eliminado";

                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "No se encontro el registro";
                    }
                }
            }
            catch (Exception ex)
            {

                response.Status = false;
                response.Message = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Metodo listar todos los Status
        /// </summary>
        /// <returns>response</returns>
        public static Utilidades.Response ListStatus()
        {
            response = new Utilidades.Response();
            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    response.Data = db.tbStatus.ToList();
                    response.Status = true;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }
            return response;
        }

        /// <summary>
        /// Metodo listar con parametro Status_id
        /// </summary>
        /// <param name="status_Id"></param>
        /// <returns>response</returns>
        public static Utilidades.Response ConsultaStatus(int status_Id)
        {
            response = new Utilidades.Response();

            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var result = (from tbStatus in db.tbStatus
                                  where tbStatus.Status_Id == status_Id
                                  select tbStatus).ToList();

                    response.Status = true;
                    response.Data = result.ToList();
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }
            return response;
        }

        /// <summary>
        /// Llena el gridView de status en el formulario Status
        /// </summary>
        /// <param name="grid"></param>
        public static void LlenarGridStatus(ref GridView grid)
        {
            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var data = (from tbStatus in db.tbStatus
                                select new
                                {
                                    Status_Id = tbStatus.Status_Id,
                                    Descripcion = tbStatus.Descripcion

                                }).ToList();

                    grid.DataSource = data;
                    grid.DataBind();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Llena un DropDownList con el listado de los Estatus
        /// </summary>
        /// <param name="ddl"></param>
        public static void llenarDropDownConStatus(ref DropDownList ddl)
        {
            try
            {

                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var data = (from i in db.tbStatus
                                select new
                                {
                                    text = i.Descripcion,
                                    value = i.Status_Id

                                }).ToList();

                    ddl.DataSource = data;
                    ddl.DataTextField = "text";
                    ddl.DataValueField = "value";
                    ddl.DataBind();

                    ddl.Items.Insert(0, new ListItem("Seleccione status", "-1"));
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace PruebaWeb.Logica.Clases
{
  public static class ClsRol
    {
        static Utilidades.Response response = null;

        /// <summary>
        /// Metodo de register un Rol 
        /// </summary>
        /// <param name="Rol"></param>
        /// <returns>response</returns>
        public static Utilidades.Response CreateRol(Entidades.tbRol Rol)
        {
            response = new Utilidades.Response();
            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {

                    db.tbRol.Add(Rol);
                    db.SaveChanges();

                    response.Status = true;
                    response.Message = "Registro exitoso";
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }
            return response;
        }

        /// <summary>
        /// Metodo de Update de Rol
        /// </summary>
        /// <param name="Rol"></param>
        /// <returns>response</returns>
        public static Utilidades.Response UpdateRol(Entidades.tbRol Rol)
        {
            response = new Utilidades.Response();

            try
            {

                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var item = db.tbRol.Find(Rol.Rol_Id) ?? null;

                    if (item != null)
                    {
                        item = Rol;
                        var x = db.SaveChanges();

                        response.Status = true;
                        response.Message = "Actualizacion exitosa";
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "No se encontro el resgistro";
                    }

                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;

            }
            return response;
        }


        /// <summary>
        /// Metodo de delete de Rol
        /// </summary>
        /// <param name="Rol_Id"></param>
        /// <returns>response</returns>
        public static Utilidades.Response DeleteRol(int Rol_Id)
        {
            response = new Utilidades.Response();
            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var item = db.tbRol.Find(Rol_Id) ?? null;

                    if (item != null)
                    {
                        db.tbRol.Remove(item);
                        db.SaveChanges();

                        response.Status = true;
                        response.Message = "Registro eliminado";

                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "No se encontro el registro";
                    }
                }
            }
            catch (Exception ex)
            {

                response.Status = false;
                response.Message = ex.Message;
            }

            return response;
        }

        /// <summary>
        /// Metodo listar todos los Roles que existen en la db
        /// </summary>
        /// <returns>response</returns>
        public static Utilidades.Response ListRol()
        {
            response = new Utilidades.Response();
            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    response.Data = db.tbRol.ToList();
                    response.Status = true;
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }
            return response;
        }

        /// <summary>
        /// Metodo consulta por parametro Rol_id
        /// </summary>
        /// <param name="Rol_Id"></param>
        /// <returns>response</returns>
        public static Utilidades.Response ConsultaRol(int Rol_Id)
        {
            response = new Utilidades.Response();

            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var result = (from tbRol in db.tbRol
                                  where tbRol.Rol_Id == Rol_Id
                                  select tbRol).ToList();

                    response.Status = true;
                    response.Data = result.ToList();
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }
            return response;
        }

        /// <summary>
        /// Llena el gridView en el formulario Rol
        /// </summary>
        /// <param name="grid"></param>
        public static void LlenarGridRol(ref GridView grid)
        {
            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var data = (from tbRol in db.tbRol
                                select new
                                {
                                    Rol_Id = tbRol.Rol_Id,
                                    Descripcion = tbRol.Descripcion

                                }).ToList();

                    grid.DataSource = data;
                    grid.DataBind();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Llena un DropDownList con el listado de los Roles
        /// </summary>
        /// <param name="ddl"></param>
        public static void llenarDropDownConRol(ref DropDownList ddl)
        {
            try
            {

                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var data = (from i in db.tbRol
                                select new
                                {
                                    text = i.Descripcion,
                                    value = i.Rol_Id

                                }).ToList();

                    ddl.DataSource = data;
                    ddl.DataTextField = "text";
                    ddl.DataValueField = "value";
                    ddl.DataBind();

                    ddl.Items.Insert(0, new ListItem("Seleccione rol", "-1"));
                }                

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}

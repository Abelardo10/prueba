﻿using PruebaWeb.Logica.Utilidades;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace PruebaWeb.Logica.Clases
{
   public static class ClsDocumento
   {
        static Utilidades.Response response = null;

        /// <summary>
        /// Llena el gridView de Documento
        /// </summary>
        /// <param name="grid"></param>
        public static void LlenarGridDocumento(ref GridView grid)
        {
            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var data = (from con in db.tbDocumento
                                select new
                                {
                                    Doccumwnto_Id = con.Documento_Id,
                                    TipoDocumento_Id = con.TipoDocumento_Id,
                                    NombreArchivo = con.NombreArchivo,
                                    Indice1 = con.Indice1,
                                    Indice2 = con.Indice2,
                                    Indice3 = con.Indice3


                                }).ToList();

                    grid.DataSource = data;
                    grid.DataBind();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Metodo de subir al servidor los archivos
        /// </summary>
        /// <param name="upload"></param>
        /// <returns></returns>
        public static Response UploadDocumento(FileUpload upload)
        {
            string nameDocument = Path.GetFileName(upload.FileName);

            Response response = new Response();

            //Verifica la extencion del archivo
            if (Path.GetExtension(upload.FileName).Equals(".pdf"))
            {

                try
                {

                    var ruta = @"C:\\Ruta\";

                    upload.SaveAs(@ruta + nameDocument);

                    using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                    {
                        Entidades.tbDocumento documento = new Entidades.tbDocumento
                        {
                            NombreArchivo = nameDocument,
                            Indice1 = ruta,
                            Indice2 = nameDocument,
                            Indice3 = @ruta + nameDocument
                        };
        
                db.tbDocumento.Add(documento);
                        db.SaveChanges();

                        response.Status = true;
                        response.Message = "Documento Cargado";
                    }
                }
                catch (Exception ex)
                {
                    response.Status = false;
                    response.Message = ex.Message;
                }
            }
            else
            {
                response.Status = false;
                response.Message = "Formato del documento  incorrecto";
            }
            return response;
        }
        
    }
}

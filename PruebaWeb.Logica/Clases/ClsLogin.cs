﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PruebaWeb.Logica.Clases
{
    public static class ClsLogin
    {
        static Utilidades.Response response = null;

        static Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities();

        /// <summary>
        /// Verifica las credenciales del usuario en la base de datos
        /// </summary>
        /// <param name="user"></param>
        /// <returns>User</returns>
        public static Utilidades.Response VerificarUsuario(Entidades.tbUsuer user)
        {
            response = new Utilidades.Response();

            try
            {
                var result = (from i in db.tbUsuer
                              where i.UserName == user.UserName && i.Pass == user.Pass
                              select i).FirstOrDefault();

                if (result != null)
                {
                    response.Status = true;
                }
                else
                {
                    response.Status = false;
                    response.Message = "Usuario ó Password incorrecto";
                }

            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }
            return response;
        }
        



    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace PruebaWeb.Logica.Clases
{
    public static class ClsEncriptacion
    {
        /// <summary>
        /// Metodo de encriptacion de password en un solo sentido MD5
        /// </summary>
        /// <param name="cadena"></param>
        /// <returns>Retorna un dato cifrado</returns>
        public static string Encriptacion(string cadena)
        {
            byte[] BtClearBytes;
            BtClearBytes = new UnicodeEncoding().GetBytes(cadena);
            byte[] BthashedBytes = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(BtClearBytes);
            string sthashedText = BitConverter.ToString(BthashedBytes);

            return sthashedText;
        }
    
    }
}

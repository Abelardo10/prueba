﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaWeb.Logica.Clases
{
   public static class ClsRegister
    {
        static Utilidades.Response response = null;


        /// <summary>
        /// Metodo para registrar usuarios
        /// </summary>
        /// <param name="Usuer"></param>
        /// <returns>response</returns>
        public static Utilidades.Response CreateUser(Entidades.tbUsuer Usuer)
        {
            response = new Utilidades.Response();
            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {

                    db.tbUsuer.Add(Usuer);
                    db.SaveChanges();

                    response.Status = true;
                    response.Message = "Registro exitoso";
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }
            return response;
        }

        /// <summary>
        /// Metodo de consulta para saber si el usuario ya se encuentra registrado
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public static Utilidades.Response CosultarUser(string Email)
        {
            response = new Utilidades.Response();

            try
            {
                using (Entidades.dbPruebaWebEntities db = new Entidades.dbPruebaWebEntities())
                {
                    var result = (from tbUsuer in db.tbUsuer
                                  where tbUsuer.UserName == Email
                                  select tbUsuer).ToList();
                    if (result == null)
                    {
                        response.Status = true;
                        response.Data = result.ToList();
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "El usuario Ya se encuentra registrado.";
                    }

                 
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaWeb.Logica.Utilidades
{
    /// <summary>
    /// Variables de retorno de mensajes como "", true, false or data de base de datos
    /// </summary>
    public class Response
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public dynamic Data { get; set; }
    }
}

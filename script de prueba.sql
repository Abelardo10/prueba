

create database dbPruebaWeb

use dbPruebaWeb

create table tbUsuer(
Usuario_Id int not null identity(1,1) primary key,
Nombres varchar(50)  null,
Apellidos varchar(50)  null,
UserName varchar(50) not null,
Pass varchar(50) not null,
Session_Id varchar(50) null,
Rol_Id int null,
Status_Id int null
)

create table tbRol(
Rol_Id int not null identity(1,1) primary key,
Descripcion varchar(50) null  
)

create table tbStatus(
Status_Id int not null identity(1,1) primary key,
Descripcion varchar(50) null
)


create table tbTipoDocumento(
TipoDocumento_Id  int not null identity(1,1) primary key,
Codigo int not null,
Nombre varchar(50) not null,
Descripcion varchar(50) not null
)

create table tbDocumento(
Documento_Id int not null identity(1,1) primary key,
TipoDocumento_Id int not null,
NombreArchivo varchar(50) not null,
FechaEmision date not null,
Indice1 varchar(50) null,
Indice2 varchar(50) null,
Indice3 varchar(50) null
)

alter table tbUsuer
add constraint FK_User_Rol
foreign key (Rol_Id) references tbRol(Rol_Id)

alter table tbUsuer
add constraint FK_User_Status
foreign key (Status_Id) references tbStatus(Status_Id)

alter table tbDocumento
add constraint FK_TipoDocumento_Documento
foreign key (TipoDocumento_Id) references tbTipoDocumento(TipoDocumento_Id)
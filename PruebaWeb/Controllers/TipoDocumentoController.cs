﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PruebaWeb.Controllers
{
    public class TipoDocumentoController : ApiController
    {
        // GET: api/TipoDocumento
        public IEnumerable<string> Index()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/TipoDocumento/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/TipoDocumento
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/TipoDocumento/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TipoDocumento/5
        public void Delete(int id)
        {
        }
    }
}

﻿using PruebaWeb.Logica.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PruebaWeb.Views.Register
{
    public partial class FrmRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                    Response.Redirect("../Login/FrmLogin.aspx");

                Initial();

            }
           
        }
        /// <summary>
        /// Registro de usuarios
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRegistar_Click(object sender, EventArgs e)
        {
            Logica.Utilidades.Response response = null;

            int faltaInfo = 0;
            string datosFaltantes = "";
            if (string.IsNullOrEmpty(txtNombres.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Nombres, "; }
            if (string.IsNullOrEmpty(txtApellidos.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Apellidos, "; }
            if (string.IsNullOrEmpty(txtUserName.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Usuario, "; }
            if (string.IsNullOrEmpty(txtPassword.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Password, "; }
            if (string.IsNullOrEmpty(Convert.ToString(ddlRol.SelectedValue))) { faltaInfo++; datosFaltantes = datosFaltantes + "Rol, "; }
            if (string.IsNullOrEmpty(Convert.ToString(ddlStatus.SelectedValue))) { faltaInfo++; datosFaltantes = datosFaltantes + "Status, "; }
            var rol = ddlRol.SelectedValue;

            if (faltaInfo > 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "mensaje", "<script> swal('Faltan los siguientes Datos:', '" + datosFaltantes + "', 'info')</script>");
            }
            else
            {
                Logica.Entidades.tbUsuer userRegister = new Logica.Entidades.tbUsuer();

                userRegister.Nombres = txtNombres.Text;
                userRegister.Apellidos = txtApellidos.Text;
                userRegister.Rol_Id = Convert.ToInt32(ddlRol.SelectedValue);
                userRegister.Status_Id = Convert.ToInt32(ddlStatus.SelectedValue);

                string cadena = txtPassword.Text;
                userRegister.Pass = ClsEncriptacion.Encriptacion(cadena);

                string user = txtUserName.Text;
                userRegister.UserName = user;

                //Instanciamos el metodo ConsultarUser y le enviamos por parametro  el User
                var respuesta = response = Logica.Clases.ClsRegister.CosultarUser(user);

                if (respuesta.Status == true)
                {

                    //Instanciamos el metodo CreateUser y le enviamos por parametro  el objeto User
                    response = Logica.Clases.ClsRegister.CreateUser(userRegister);

                    Response.Write("<script>alert('" + response.Message + "')</script>");



                    //Limpiamos los campos del formulario
                    txtNombres.Text = "";
                    txtApellidos.Text = "";
                    txtUserName.Text = "";
                    txtPassword.Text = "";
                    ddlRol.SelectedValue = "-1";
                    ddlStatus.SelectedValue = "-1";
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "mensaje", "<script> swal('ooh!', '" + response.Message + "', 'error')</script>");
                    Response.Redirect("../Login/FrmLogin.aspx");
                }
            }
        }

        /// <summary>
        //Llena los dorpdownlist Rol & Status desde la base de datos
        /// </summary>
        public void Initial()
        {
          ClsRol.llenarDropDownConRol(ref ddlRol);
          ClsStatus.llenarDropDownConStatus(ref ddlStatus);

        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Home.Master" AutoEventWireup="true" CodeBehind="FrmRegister.aspx.cs" Inherits="PruebaWeb.Views.Register.FrmRegister" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../../Content/sweetalert.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/sweetalert.min.js" type="text/javascript"></script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container" >
        <div class="col-sm-3">
        </div>
        <div class="col-sm-6">        
        <div class="panel-body">            
                 <div class="panel panel-primary center-block" > 
                    <div class="panel-footer" ><h5><center>Register's Users</center></h5></div>
                        <div class="panel-body" >
                            <div class="row" style="margin-top:-1em"> 
                                <div class="col-md-12" >  Nombres    
                                     <asp:TextBox ID="txtNombres" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                             <div class="row" style="margin-top:-1em"> 
                                <div class="col-md-12" >  Apellidos  
                                     <asp:TextBox ID="txtApellidos" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row" style="margin-top:-1em"> 
                                <div class="col-md-12" >  Usuario  
                                     <asp:TextBox type="email" ID="txtUserName" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row" style="margin-top:-1em"> 
                                <div class="col-md-12" >  Password  
                                     <asp:TextBox type="password" ID="txtPassword" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row" style="margin-top:-1em">
                              <div class="col-md-12" >  Rol
                                <asp:dropdownlist runat="server" CssClass="form-control" id="ddlRol">

                                </asp:dropdownlist>
                              </div>
                            </div>

                             <div class="row" style="margin-top:-1em">
                              <div class="col-md-12" >  Status
                                <asp:dropdownlist runat="server" CssClass="form-control" id="ddlStatus">

                                </asp:dropdownlist>
                              </div>
                            </div>
                       
                 </div>
                    <div class="panel-collapse center-block" >
                         <div class="panel-body">
                             <div class="row">
                                <div class="col-md-12" style="margin-top:-4em"> 
                                    <div class="pager">                            
                                        <asp:Button ID="btnRegistar" Width="70%" runat="server" Text="Registrar" CssClass="btn btn-primary" OnClick="btnRegistar_Click" ></asp:Button>
                                    </div>

                                 </div>
                             </div>
                         </div>
                    </div> 
                </div> <!--Cierre del panel body -->          
        

       
        </div>
        </div>

        <div class="col-sm-3">
        </div>
        </div>
</asp:Content>

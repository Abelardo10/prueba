﻿using PruebaWeb.Logica.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PruebaWeb.Views.Rol
{
    public partial class FrmRolUpdate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login"] == null)
                Response.Redirect("../Login/FrmLogin.aspx");

            if (Request.QueryString["id"] == null) return;
            {
                int id = int.Parse(Request.QueryString["id"].ToString());
                consultaRol(id);
            }
        }

        /// <summary>
        /// Atualiza un rol
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            Logica.Utilidades.Response response = null;

            tbRol b = new tbRol();
            try
            {
                b.Rol_Id = int.Parse(txtId.Text);               
                b.Descripcion = txtDescripcion.Text;

                if (b != null)
                {

                    response = Logica.Clases.ClsRol.UpdateRol(b);

                    Response.Write("<script>alert('" + response.Message + "')</script>");
                    Response.Redirect("FrmRol.aspx");
                }

            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }

        }
        /// <summary>
        /// Redirecionna a la vista Rol
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("FrmRol.aspx");
        }

        /// <summary>
        /// Consulta un Rol a partir de un id
        /// </summary>
        /// <param name="Id"></param>
        public void consultaRol(int Id)
        {
            Logica.Utilidades.Response response = null;

            try
            {
                response = Logica.Clases.ClsRol.ConsultaRol(Id);

                //Estamos castiando la variable response.Data
                var lista = (List<tbRol>)response.Data;


                if (lista != null)
                {

                    //desintegramos la variable data
                    txtId.Text = Convert.ToString(lista[0].Rol_Id);
                    txtDescripcion.Text = Convert.ToString(lista[0].Descripcion);

                }



            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}
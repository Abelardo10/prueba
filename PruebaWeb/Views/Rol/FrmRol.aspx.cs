﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PruebaWeb.Views.Rol
{
    public partial class FrmRol : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                    Response.Redirect("../Login/FrmLogin.aspx");

                BindGrid();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRegistar_Click(object sender, EventArgs e)
        {
            Logica.Utilidades.Response response = null;

            int faltaInfo = 0;
            string datosFaltantes = "Faltan los siguientes Datos: ";
            if (string.IsNullOrEmpty(txtDescripcion.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Descripción, "; }
           


            if (faltaInfo > 0)
            {
                Response.Write("<script>alert('" + datosFaltantes + "')</script>");
            }
            else
            {
                Logica.Entidades.tbRol rol = new Logica.Entidades.tbRol();

                rol.Descripcion = txtDescripcion.Text;
               


                //Instanciamos el metodo CreateRol y le enviamos por parametro  el objeto Rol
                response = Logica.Clases.ClsRol.CreateRol(rol);

                Response.Write("<script>alert('" + response.Message + "')</script>");

                //Actualizamos la Grilla
                BindGrid();

                //Limpiamos los campos del formulario
                txtDescripcion.Text = "";
               

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gridRol_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gridRol.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception)
            {
                Session.Abandon();
                Response.Redirect("DashBoard.aspx");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gridRol_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                Logica.Utilidades.Response response = null;

                int inIndice = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName == "Eliminar")
                {
                    int codigo = int.Parse(gridRol.Rows[inIndice].Cells[1].Text);

                    //Instanciamos el metodo CreateTipoDocumento y le enviamos por parametro  el  codigo
                    response = Logica.Clases.ClsRol.DeleteRol(codigo);

                    Response.Write("<script>alert('" + response.Message + "')</script>");

                    BindGrid();

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gridRol_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Id = gridRol.SelectedRow.Cells[1].Text;
            Response.Redirect("FrmRolUpdate.aspx?id=" + Id);
        }
        /// <summary>
        /// 
        /// </summary>
        public void BindGrid()
        {
            Logica.Clases.ClsRol.LlenarGridRol(ref gridRol);
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Home.Master" AutoEventWireup="true" CodeBehind="FrmRolUpdate.aspx.cs" Inherits="PruebaWeb.Views.Rol.FrmRolUpdate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../../Content/sweetalert.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/sweetalert.min.js" type="text/javascript"></script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="container">

          
       <%-- INICIO PRIMER PANEL --%>
        <div class="panel panel-primary"> 
          <div class="panel-footer" ><h5><center>Rol</center></h5></div>
            <div class="panel-body">
                <div class="row" style="margin-top:1em">
                    <div class="row" style="margin-top:-1em"> 
                                <div class="col-md-12" >      
                                     <asp:TextBox ID="txtId" CssClass="hidden" runat="server"></asp:TextBox>
                                </div>
                            </div>
                   
                    <div class="col-md-6" style="margin-top:1em"> Descripción        
                        <asp:TextBox ID="txtDescripcion" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                 </div> 
              </div>        
            </div>       
        </div>  <%-- FIN DEL PRIMER PANEL --%>
       
   <%-- BOTON GENERAR --%>
            
             <div class=" panel-primary center-block" style="width:50%">
                        <div class="panel-default">
                        <div class="panel-body">
                <div class="row">

                 <div class="col-md-6" style="margin-top:0em">            
                     <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" CssClass="form-control btn btn-primary" OnClick="btnActualizar_Click"></asp:Button>
                 </div>                
                 <div class="col-md-6" style="margin-top:0em">                              
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="form-control btn btn-danger" OnClick="btnCancelar_Click"></asp:Button>
                </div>

              </div>
            </div>
            </div>
             </div>
</asp:Content>

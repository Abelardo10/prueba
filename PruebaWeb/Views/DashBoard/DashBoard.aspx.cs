﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PruebaWeb.Views.DashBoard
{
    public partial class DashBoard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
               
                Response.Redirect("../Login/FrmLogin.aspx");
               
                BindGrid();
            }
        }

        //LLena la griglla con todos los documentos archivados
        public void BindGrid()
        {
            Logica.Clases.ClsDocumento.LlenarGridDocumento(ref gridDocumentos);
        }

        protected void gridLeads_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void gridLeads_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        
    }
}
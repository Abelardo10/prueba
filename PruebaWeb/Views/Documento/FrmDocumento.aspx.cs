﻿using PruebaWeb.Logica.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PruebaWeb.Views.Documento
{
    public partial class FrmDocumento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                    Response.Redirect("../Login/FrmLogin.aspx");

                Initial();
            }

        }

        protected void btnRegistar_Click(object sender, EventArgs e)
        {
            Logica.Utilidades.Response response = null;

            int faltaInfo = 0;
            string datosFaltantes = "";
            if (string.IsNullOrEmpty(ddlTipoDocumento.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Tipo de Documento, "; }
            if (string.IsNullOrEmpty(txtArchivo.FileName)) { faltaInfo++; datosFaltantes = datosFaltantes + "Nombre, "; }
            if (string.IsNullOrEmpty(txtIndice1.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Descripción, "; }
            if (string.IsNullOrEmpty(txtIndice2.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Descripción, "; }
            if (string.IsNullOrEmpty(txtIndice3.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Descripción, "; }


            if (faltaInfo > 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "mensaje", "<script> swal('Faltan los siguientes Datos:', '" + datosFaltantes + "', 'info')</script>");
            }
            else
            {
                Logica.Entidades.tbDocumento file = new Logica.Entidades.tbDocumento();

                file.TipoDocumento_Id = Convert.ToInt32(ddlTipoDocumento.Text);
                // FileUploadDocumento.NombreArchivo = txtArchivo.;
                file.Indice1 = txtIndice1.Text;
                file.Indice2 = txtIndice2.Text;
                file.Indice3 = txtIndice3.Text;

                //Instanciamos el metodo CreateTipoDocumento y le enviamos por parametro  el objeto TioDocumento
               // response = ClsDocumento.UploadDocumento(FileUploadDocumento);

                ClientScript.RegisterStartupScript(this.GetType(), "mensaje", "<script> swal('Good!', '" + response.Message + "', 'info')</script>");

                //Limpiamos los campos del formulario
                ddlTipoDocumento.Text = "-1";
                //txtArchivo. = "";
                txtIndice1.Text = "";
                txtIndice2.Text = "";
                txtIndice3.Text = "";
            }
        }

        /// <summary>
        //Llena el dorpdownlist TipoDocumento desde la base de datos
        /// </summary>
        public void Initial()
        {
            ClsTipoDocumento.llenarDropDownConDocumento(ref ddlTipoDocumento);


        }
    }
}
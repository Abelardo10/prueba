﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Home.Master" AutoEventWireup="true" CodeBehind="FrmDocumento.aspx.cs" Inherits="PruebaWeb.Views.Documento.FrmDocumento" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Content/sweetalert.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/sweetalert.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container" >
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8">        
        <div class="panel-body">            
                 <div class="panel panel-primary center-block" > 
                    <div class="panel-footer" ><h5><center>Register's Documents</center></h5></div>
                        <div class="panel-body" >

                            <div class="row" style="margin-top:-1em">
                              <div class="col-md-12" > Subir documento
                                <asp:dropdownlist runat="server" CssClass="form-control" id="ddlTipoDocumento">

                                </asp:dropdownlist>
                              </div>
                            </div>

                            <div class="row" style="margin-top:-1em"> 
                                <div class="col-md-12" >  Archivo   
                                     <asp:FileUpload ID="txtArchivo" CssClass="form-control" runat="server"></asp:FileUpload>
                                </div>
                            </div>
                             <div class="row" style="margin-top:-1em"> 
                                <div class="col-md-12" >  Indice1  
                                     <asp:TextBox ID="txtIndice1" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                               <div class="row" style="margin-top:-1em"> 
                                <div class="col-md-12" >  Indice2  
                                     <asp:TextBox  ID="txtIndice2" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                               <div class="row" style="margin-top:-1em"> 
                                <div class="col-md-12" >  Indice3  
                                     <asp:TextBox  ID="txtIndice3" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            
                          </div>
                 </div>
                    <div class="panel-collapse center-block" >
                         <div class="panel-body">
                             <div class="row">
                                <div class="col-md-12" style="margin-top:-4em"> 
                                    <div class="pager">                            
                                        <asp:Button ID="btnRegistar" Width="70%" runat="server" Text="Registrar" CssClass="btn btn-primary" OnClick="btnRegistar_Click" ></asp:Button>
                                    </div>

                                 </div>
                             </div>
                         </div>
                    </div> 
                </div> <!--Cierre del panel body -->          
        

       
        </div>
        </div>

        <div class="col-sm-2">
        </div>
        </div>
</asp:Content>

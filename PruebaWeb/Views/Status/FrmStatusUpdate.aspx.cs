﻿using PruebaWeb.Logica.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PruebaWeb.Views.Status
{
    public partial class FrmStatusUpdate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login"] == null)
                Response.Redirect("../Login/FrmLogin.aspx");

            if (Request.QueryString["id"] == null) return;
            {
                int id = int.Parse(Request.QueryString["id"].ToString());
                consultaStatus(id);
            }
        }

        /// <summary>
        /// Consulta un status a partir de un Id
        /// </summary>
        /// <param name="Id"></param>
        public void consultaStatus(int Id)
        {
            Logica.Utilidades.Response response = null;

            try
            {
                response = Logica.Clases.ClsStatus.ConsultaStatus(Id);

                //Estamos castiando la variable response.Data
                var lista = (List<tbStatus>)response.Data;

                if (lista != null)
                {
                    //desintegramos la variable data
                    txtId.Text = Convert.ToString(lista[0].Status_Id);
                    txtDescripcion.Text = Convert.ToString(lista[0].Descripcion);
                }

            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }

        }

        /// <summary>
        /// Actualisa un Status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            Logica.Utilidades.Response response = null;

            tbStatus b = new tbStatus();
            try
            {
                b.Status_Id = int.Parse(txtId.Text);
                b.Descripcion = txtDescripcion.Text;

                if (b != null)
                {

                    response = Logica.Clases.ClsStatus.UpdateStatus(b);

                    Response.Write("<script>alert('" + response.Message + "')</script>");
                    Response.Redirect("FrmStatus.aspx");
                }

            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }
        }

        /// <summary>
        /// Retorna a la vista Status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("FrmStatus.aspx");
        }
    }
}
﻿using PruebaWeb.Logica.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PruebaWeb.Views.TipoDocumento
{
    public partial class FrmTipoDocuentoUpdate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                    Response.Redirect("../Login/FrmLogin.aspx");

                if (Request.QueryString["codigo"] == null) return;
                {
                 int Codigo = int.Parse(Request.QueryString["codigo"].ToString());                            
                  consulta(Codigo);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            Logica.Utilidades.Response response = null;

            tbTipoDocumento b = new tbTipoDocumento();
            try
            {

               
                b.TipoDocumento_Id = int.Parse(txtId.Text);
                b.Codigo = int.Parse(txtCodigo.Text);
                b.Nombre = txtNombre.Text;
                b.Descripcion = txtDescripcion.Text;

                if (b != null)
                {

                     response = Logica.Clases.ClsTipoDocumento.UpdateTipoDocumento(b);

                    ClientScript.RegisterStartupScript(this.GetType(), "mensaje", "<script> swal('Good job!', '" + response.Message + "', 'succes')</script>");
                    Response.Redirect("FrmTipoDocumento.aspx");


                }
                
                               
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;

            }
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("FrmTipoDocumento.aspx");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Codigo"></param>
        public void consulta(int Codigo)
        {
            Logica.Utilidades.Response response = null;

            try
            {
                 response = Logica.Clases.ClsTipoDocumento.Consulta(Codigo);

                var lista = (List<tbTipoDocumento>)response.Data;


                if (lista != null)
                {

                    //desintegramos la variable data
                    txtId.Text = Convert.ToString(lista[0].TipoDocumento_Id);

                    txtCodigo.Text = Convert.ToString (lista[0].Codigo);
                   
                    txtNombre.Text = Convert.ToString(lista[0].Nombre);

                    txtDescripcion.Text = Convert.ToString(lista[0].Descripcion);

                }
               


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

    }
}
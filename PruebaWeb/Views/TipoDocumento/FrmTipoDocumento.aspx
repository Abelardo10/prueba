﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Home.Master" AutoEventWireup="true" CodeBehind="FrmTipoDocumento.aspx.cs" Inherits="PruebaWeb.Views.TipoDocumento.FrmTipoDocumento" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../../Content/sweetalert.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/sweetalert.min.js" type="text/javascript"></script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container" >
        <div class="col-sm-3">
        </div>
        <div class="col-sm-6">        
        <div class="panel-body">            
                 <div class="panel panel-primary center-block" > 
                    <div class="panel-footer" ><h5><center>Tipo de Documento</center></h5></div>
                        <div class="panel-body" >
                            <div class="row" style="margin-top:-1em"> 
                                <div class="col-md-12" >      
                                     <asp:TextBox ID="txtId" CssClass="hidden" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row" style="margin-top:-1em"> 
                                <div class="col-md-12" > Codigo       
                                     <asp:TextBox ID="txtCodigo" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="row" style="margin-top:-1em"> 
                                <div class="col-md-12" > Nombre del documento       
                                     <asp:TextBox ID="txtNombre" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                             <div class="row" style="margin-top:-1em"> 
                                <div class="col-md-12" > Descripcion       
                                     <asp:TextBox ID="txtDescripcion" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                 </div>
                    <div class="panel-collapse center-block" >
                         <div class="panel-body">
                             <div class="row">
                                <div class="col-md-12" style="margin-top:-4em"> 
                                    <div class="pager">                            
                                        <asp:Button ID="btnRegistar" Width="70%" runat="server" Text="Registrar" CssClass="btn btn-primary" OnClick="btnRegistar_Click"></asp:Button>
                                    </div>

                                 </div>
                             </div>
                         </div>
                    </div> 
                </div> <!--Cierre del panel body -->

            
        

        <div class="panel center-block" style="margin-top:-4em" >
            <asp:GridView ID="gridTipoDocumento" runat="server" AutoGenerateColumns="false" EmptyDataText="No se encontro registro" AllowPaging="True" BackColor="White" BorderColor="#FBFCFC" BorderStyle="Solid" BorderWidth="1px" CellPadding="5"  CssClass="table table-responsive" Font-Size="Medium"   PageSize="4" OnSelectedIndexChanged="gridTipoDocumento_SelectedIndexChanged" OnPageIndexChanging="gridTipoDocumento_PageIndexChanging" OnRowCommand="gridTipoDocumento_RowCommand">
                     <Columns>
                        
                        <asp:ButtonField ButtonType="Button" CommandName="select" InsertVisible="False" Text="➤" HeaderText="Actualizar">
                            <ControlStyle CssClass="form-control" BackColor="#2874A6" Font-Bold="true" ForeColor="#F7F9F9"/>
                        </asp:ButtonField>
                         
                         <asp:BoundField DataField="Codigo" HeaderText="Código" />
                         <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                         <asp:BoundField DataField="Descripcion" HeaderText="Descripción" />

                         <asp:TemplateField HeaderText="Eliminar">
                             <ItemTemplate>
                                 <asp:Button id="Eliminar" runat="server" Text="Eliminar" CommandName="Eliminar" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />
                             </ItemTemplate>
                         </asp:TemplateField>
                    </Columns>
                     
                                            
                    <FooterStyle BackColor="#F7F9F9" ForeColor="#F7F9F9" />
                    <HeaderStyle BackColor="#337ab7" Font-Bold="True" ForeColor="#F7F9F9" CssClass="DataGridFixedHeader" />
                    <PagerStyle CssClass="gridViewPager" BackColor="#2874A6" ForeColor="#F7F9F9" HorizontalAlign="Center" Font-Bold="true" />
                    <RowStyle ForeColor="#337ab7" BackColor="White" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SortedAscendingCellStyle BackColor="#F7F9F9" />
                    <SortedAscendingHeaderStyle BackColor="#F7F9F9" />
                    <SortedDescendingCellStyle BackColor="#F7F9F9" />
                    <SortedDescendingHeaderStyle BackColor="#F7F9F9" />
                </asp:GridView>

        </div>
        </div>
            </div>

        <div class="col-sm-3">
        </div>

      </div>

</asp:Content>

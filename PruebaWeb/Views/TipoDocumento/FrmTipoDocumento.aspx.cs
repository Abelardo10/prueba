﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace PruebaWeb.Views.TipoDocumento
{
    public partial class FrmTipoDocumento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             if (!IsPostBack)
            {
                if (Session["Login"] == null)
                    Response.Redirect("../Login/FrmLogin.aspx");
                BindGrid();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRegistar_Click(object sender, EventArgs e)
        {
            Logica.Utilidades.Response response = null;

            int faltaInfo = 0;
            string datosFaltantes = "";
            if (string.IsNullOrEmpty(txtCodigo.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Codigo, "; }
            if (string.IsNullOrEmpty(txtNombre.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Nombre, "; }
            if (string.IsNullOrEmpty(txtDescripcion.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Descripción, "; }


            if (faltaInfo > 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "mensaje", "<script> swal('Faltan los siguientes Datos:', '" + datosFaltantes + "', 'info')</script>");
            }
            else
            {
                Logica.Entidades.tbTipoDocumento TioDocumento = new Logica.Entidades.tbTipoDocumento();

                TioDocumento.Codigo = Convert.ToInt32(txtCodigo.Text);
                TioDocumento.Nombre = txtNombre.Text;
                TioDocumento.Descripcion = txtDescripcion.Text;


                //Instanciamos el metodo CreateTipoDocumento y le enviamos por parametro  el objeto TioDocumento
                response = Logica.Clases.ClsTipoDocumento.CreateTipoDocumento(TioDocumento);

                ClientScript.RegisterStartupScript(this.GetType(), "mensaje", "<script> swal('Good!', '" + response.Message + "', 'success')</script>");

                //Actualizamos la Grilla
                BindGrid();
            
                //Limpiamos los campos del formulario
                txtCodigo.Text = "";
                txtNombre.Text = "";
                txtDescripcion.Text = "";

            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public void BindGrid()
        {
            
            Logica.Clases.ClsTipoDocumento.LlenarGridTipoDocumento(ref gridTipoDocumento);
            
        }

              
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gridTipoDocumento_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gridTipoDocumento.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception)
            {
                Session.Abandon();
                Response.Redirect("DashBoard.aspx");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gridTipoDocumento_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Codigo = gridTipoDocumento.SelectedRow.Cells[1].Text;

            Response.Redirect("FrmTipoDocuentoUpdate.aspx?codigo=" + Codigo);
        }

        protected void gridTipoDocumento_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                Logica.Utilidades.Response response = null;

                int inIndice = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName == "Eliminar")
                {
                      int codigo = int.Parse(gridTipoDocumento.Rows[inIndice].Cells[1].Text);

                    //Instanciamos el metodo CreateTipoDocumento y le enviamos por parametro  el  codigo
                    response = Logica.Clases.ClsTipoDocumento.DeleteTipoDocumento(codigo);

                    ClientScript.RegisterStartupScript(this.GetType(), "mensaje", "<script> swal('Good!', '" + response.Message + "', 'success')</script>");

                    BindGrid();

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
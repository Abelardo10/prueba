﻿using PruebaWeb.Logica.Clases;
using PruebaWeb.Logica.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PruebaWeb.Views.Login
{
    public partial class FrmLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.Cookies["Token"] != null)
                    txtEmail.Text = Request.Cookies["Token"].Value;
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Logica.Utilidades.Response response = null;

            try
            {
           
            int faltaInfo = 0;
            string datosFaltantes = "Faltan los siguientes Datos: ";
            if (string.IsNullOrEmpty(txtEmail.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Email, "; }
            if (string.IsNullOrEmpty(txtPassword.Text)) { faltaInfo++; datosFaltantes = datosFaltantes + "Password, "; }

                if (faltaInfo > 0)
            {
                    ClientScript.RegisterStartupScript(this.GetType(), "mensaje", "<script> swal('Faltan los siguientes Datos:', '" + datosFaltantes + "', 'info')</script>");
                   
            }
            else
            {
                Logica.Entidades.tbUsuer user = new Logica.Entidades.tbUsuer();               


                    user.UserName = txtEmail.Text;              
                    string cadena = txtPassword.Text;
                    user.Pass=   ClsEncriptacion.Encriptacion(cadena);


                    //Instanciamos el metodo Verificar usuario y le enviamos por parametro  el objeto user
                  response =   Logica.Clases.ClsLogin.VerificarUsuario(user);

                    //var lista = (List<tbUsuer>)response.Data;

                    if (response.Status != false)
                    {
                        //Manejo de Cookie si el user desea recordar el usuario
                        if (chkRecordar.Checked)
                        {
                            HttpCookie cookie = new HttpCookie("Token", txtEmail.Text);
                            cookie.Expires = DateTime.Now.AddDays(2);
                            this.Response.Cookies.Add(cookie);
                        }
                        else
                        {
                            HttpCookie cookie = new HttpCookie("Token", txtEmail.Text);
                            cookie.Expires = DateTime.Now.AddDays(-1);
                            this.Response.Cookies.Add(cookie);
                        }

                        //Manejo de las sesiones
                        Session["Login"] = txtEmail.Text;                        
                        Response.Redirect("../DashBoard/DashBoard.aspx");

                    }
                    else {
                        
                        ClientScript.RegisterStartupScript(this.GetType(), "mensaje", "<script> swal('ooh!', '" + response.Message + "', 'error')</script>");
                                                                       
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
            }
        }
    }
}
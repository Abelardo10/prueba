﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmLogin.aspx.cs" Inherits="PruebaWeb.Views.Login.FrmLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Login</title>
     
    <link href="../../Content/LoginStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/sweetalert.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/sweetalert.min.js" type="text/javascript"></script> 

</head>
<body>
   <div class="container">
    <div class="row">
        <div class="col-md-12">           
            
            <div class="panel-body">
                <p 
                    class="form-title">
                    Login

                </p>
                <form class="login" id="formLogin" method="post" runat="server">

                    <asp:TextBox ID="txtEmail" runat="server" type="email" placeholder="Email" class="form-control" required="required"></asp:TextBox>    
                    
                    <br />

                    <asp:TextBox type="password" placeholder="Password" id="txtPassword" class="form-control" required="required" runat="server"></asp:TextBox>


                    <asp:Button ID="btnLogin" runat="server"  Text="Sign In" CssClass="btn btn-success btn-sm" OnClick="btnLogin_Click"> </asp:Button>
                
                   
                    <div class="remember-forgot">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    <asp:CheckBox ID="chkRecordar" runat="server" />
                                     Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 forgot-pass-content">
                            <a href="#" class="forgot-pass">Forgot Password</a>
                        </div>
                    </div>
                </div>
                </form>
            </div>
          </div>
    </div>
   </div>
</body>
</html>
